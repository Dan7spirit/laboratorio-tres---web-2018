<?php
  require 'database.php';
  $message = '';
  if (!empty($_POST['id']) && !empty($_POST['name']) && !empty($_POST['password']) && !empty($_POST['confirm_password'])) {
    if($_POST['password'] == $_POST['confirm_password']){
      $verifyID = "SELECT nombre FROM Miembro where cedula = :id LIMIT 1";
      $stmtVerify = $conn->prepare($verifyID);
      $stmtVerify->bindParam(':id', $_POST['id']);
      $stmtVerify->execute();
      if($stmtVerify->fetchColumn()){
        $message = 'Ya existe un miembro con esa cédula';
      }else{
        $sql = "INSERT INTO miembro VALUES (:id,:name,:password,:birth_date, 'Y','M')";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $_POST['id']);
        $stmt->bindParam(':name', $_POST['name']);
        $stmt->bindParam(':password', $_POST['password']);
        $stmt->bindParam(':birth_date', $_POST['birth_date']);
        if ($stmt->execute()) {
          $message = 'Se ha registrado exitosamente el usuario';
        } else {
          $message = 'Ha ocurrido un error';
        }
      }
    }else{
      $message = 'Contraseñas no coinciden';
    }
    
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Registro de miembros</title>
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- Materialize-->
    <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

      <!-- Compiled and minified JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

  </head>
  <body>

    <?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>

    <h1>Bienvenido al registro de Miembros</h1>
    <span>or <a href="login.php">Login</a></span>

    <form action="signup.php" method="POST">
      <input name="id" type="text" placeholder="Ingrese su cédula">
      <input name="name" type="text" placeholder="Ingrese su nombre">
      <input name="password" type="password" placeholder="Ingrese su contraseña">
      <input name="confirm_password" type="password" placeholder="Confirme contraseña">
      <input type="date" name="birth_date">
      <input type="submit" value="Submit">
    </form>

  </body>
</html>