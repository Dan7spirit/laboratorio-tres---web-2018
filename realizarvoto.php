<?php
	session_start();
	require 'database.php';

	if(isset($_SESSION['user_id']) && isset($_SESSION['propuestaNum'])){
		$answer = $_POST['voto'];
		$voto = "F";
		if($answer == "contra"){
			$voto = "C";
		}
		if($answer == "abstengo"){
			$voto = "A";
		}

		$sql = "SELECT voto from votoporpropuesta where cedula = :cedula and propuestanum = :propuestanum";
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':cedula', $_SESSION['user_id']);
		$stmt->bindParam(':propuestanum', $_SESSION['propuestaNum']);
		$stmt->execute();
		if ($stmt->fetchColumn()) {
      		$newSQL = "UPDATE votoporpropuesta set voto = :voto where cedula = :cedula and propuestanum = :propuestanum";
      		$newStmt = $conn->prepare($newSQL);
      		$newStmt->bindParam(':cedula', $_SESSION['user_id']);
			$newStmt->bindParam(':propuestanum', $_SESSION['propuestaNum']);
			$newStmt->bindParam(':voto', $voto);
			$newStmt->execute();
			header('Location: /LaboratorioTres_DanielS/index.php');
    	} else {
      		$newSQL = "INSERT INTO votoporpropuesta values(:propuestanum,:cedula,:voto)";
      		$newStmt = $conn->prepare($newSQL);
      		$newStmt->bindParam(':cedula', $_SESSION['user_id']);
			$newStmt->bindParam(':propuestanum', $_SESSION['propuestaNum']);
			$newStmt->bindParam(':voto', $voto);
			$newStmt->execute();
			header('Location: /LaboratorioTres_DanielS/index.php');
    	}
	}
?>