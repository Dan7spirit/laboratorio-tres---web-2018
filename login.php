<?php
  session_start();
  require 'database.php';
  if (!empty($_POST['id']) && !empty($_POST['password'])) {
    $verifyID = "SELECT nombre FROM Miembro where cedula = :id and contrasenna = :password LIMIT 1";
    $stmtVerify = $conn->prepare($verifyID);
    $stmtVerify->bindParam(':id', $_POST['id']);
    $stmtVerify->bindParam(':password', $_POST['password']);
    $stmtVerify->execute();
    $message = '';
    if ($stmtVerify->fetchColumn()) {
      $_SESSION['user_id'] = $_POST['id'];
      $message="Bienvenido";
      header('Location: /LaboratorioTres_DanielS/index.php');
    } else {
      $message = 'Los credenciales no coinciden';
    }
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Iniciar sesión</title>
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- Materialize-->
    <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

      <!-- Compiled and minified JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

  </head>
  <body>
    <?php if(!empty($message)): ?>
      <p> <?= $message ?></p>
    <?php endif; ?>

    <h1>Login</h1>
    <span>or <a href="signup.php">SignUp</a></span>

    <form action="login.php" method="POST">
      <input name="id" type="text" placeholder="Ingrese su cédula">
      <input name="password" type="password" placeholder="Ingrese su contraseña">
      <input type="submit" value="Submit">
    </form>
  </body>
</html>