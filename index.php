<?php
	session_start();
	unset($_SESSION["propuestaNum"]);
	$user = null;
	if(isset($_SESSION['user_id'])){
		$user = $_SESSION['user_id'];
	}
?>

<!DOCTYPE html>
<html lang="es" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>Bienvenido</title>
		<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <!-- Materialize-->
    <!-- Compiled and minified CSS -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

      <!-- Compiled and minified JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
	</head>
	<body>
	<?php if(!empty($user)): ?>
      <br> Bienvenido
      <a href="logout.php">Cerrar sesión </a>
        <?php
        	require 'database.php';
        	$sql = "SELECT propuestanum,propuestaid FROM propuestavotacion where estado = 'V'";
     		$data = $conn->query($sql);
      		echo '<ul>';
      		foreach($data as $row){
      			$secondSQL = "SELECT titulo, descripcion from propuestarevision where propuestaid = :idpropuesta";
      			$secondStmt = $conn->prepare($secondSQL);
    			$secondStmt->bindParam(':idpropuesta', $row[0]);
			    $secondStmt->execute();
			    $results = $secondStmt->fetch();
      			echo "<form action='votar.php' method='POST'>";
			      echo "<input name='propuestanum' type='hidden' placeholder='' value=$row[0]>";
			      echo "<input name='propuestatit' type='text' placeholder='' value = $results[0]>";
			      echo "<input name='propuestades' type='text' placeholder='' value= $results[1]>";
			      echo "<input type='submit' value='Submit'>";
			    echo '</form>';
			    echo "<br>";
      		}
      		echo '</ul>';
      	?>
    <?php else: ?>
      <h1>Por favor, inicie sesión o regístrese</h1>
      <a href="login.php">Iniciar Sesión</a> or
      <a href="signup.php">Registrarse</a>
	<?php endif; ?>
	</body>
</html>
